import { Component } from '@angular/core';

@Component({
  selector: 'planner-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'planner';
}
